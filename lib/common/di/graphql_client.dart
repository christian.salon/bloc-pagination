import 'package:graphql_flutter/graphql_flutter.dart';

// Rick and Morty API
// https://studio.apollographql.com/public/rick-and-morty-a3b90u/variant/current/home
final _httpLink = HttpLink('https://rickandmortyapi.com/graphql');

final GraphQLClient graphQLClient = GraphQLClient(
  cache: GraphQLCache(),
  link: Link.from([_httpLink]),
  defaultPolicies: DefaultPolicies(
    query: Policies.safe(
      FetchPolicy.networkOnly,
      ErrorPolicy.none,
      CacheRereadPolicy.ignoreAll,
    ),
  ),
);
