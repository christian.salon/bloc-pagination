import '../interface/episode_interface.dart';
import '../model/output/episode.dart';
import '../remote/episode_remote_source.dart';

class EpisodeRepository implements EpisodeInterface {
  final EpisodeRemoteSource _remoteSource;

  const EpisodeRepository(this._remoteSource);

  @override
  Future<List<Episode>> getEpisodes({required int page}) async {
    return await _remoteSource.getEpisodes(page);
  }
}
