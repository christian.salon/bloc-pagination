import '../model/output/episode.dart';

abstract class EpisodeInterface {
  Future<List<Episode>> getEpisodes({required int page});
}
