import '../../../common/di/graphql_client.dart';
import '../remote/episode_remote_source.dart';
import '../repository/episode_repository.dart';

final episodeRemoteSource = EpisodeRemoteSource(graphQLClient);
final episodeRepository = EpisodeRepository(episodeRemoteSource);
