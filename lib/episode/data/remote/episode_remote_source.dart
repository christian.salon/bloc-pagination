import 'package:graphql_flutter/graphql_flutter.dart';

import '../model/output/episode.dart';

enum _QueryName {
  episodes,
}

class EpisodeRemoteSource {
  final GraphQLClient _graphQLClient;

  const EpisodeRemoteSource(this._graphQLClient);

  static const _episodesQuery = r'''
    query Episodes($page: Int) {
      episodes(page: $page) {
        results {
          id
          name
          air_date
          episode
        }
      }
    }
  ''';

  Future<List<Episode>> getEpisodes(int page) async {
    final options = QueryOptions(
      document: gql(_episodesQuery),
      variables: {'page': page},
    );

    final response = await _graphQLClient.query(options);

    if (!response.hasException) {
      final data = response.data![_QueryName.episodes.name]['results']
          as List<dynamic>;

      return data.map((e) => Episode.fromJson(e)).toList();
    } else {
      throw response.exception!;
    }
  }
}
