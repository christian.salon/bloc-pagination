import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class Episode extends Equatable {
  final String name;
  final String airDate;
  final String episode;
  final String created;

  const Episode({
    required this.name,
    required this.airDate,
    required this.episode,
    required this.created,
  });

  factory Episode.fromJson(Map<String, dynamic> json) {
    return Episode(
      name: json['name'] as String? ?? '',
      airDate: json['air_date'] as String? ?? '',
      episode: json['episode'] as String? ?? '',
      created: json['created'] as String? ?? '',
    );
  }

  @override
  List<Object?> get props => [name, airDate, episode, created];
}
