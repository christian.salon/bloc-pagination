import 'package:bloc_pagination/episode/data/di/episode_service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../component/episode_view.dart';
import '../event/episode_event.dart';
import '../view_model/episodes_view_model.dart';

class EpisodeScreen extends StatelessWidget {
  const EpisodeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Episodes'),
      ),
      body: BlocProvider(
        create: (_) => EpisodesViewModel(
          repository: episodeRepository,
        )..add(EpisodesFetched()),
        child: const EpisodeView(),
      ),
    );
  }
}
