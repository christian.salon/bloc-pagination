import 'package:bloc/bloc.dart';

import '../../data/interface/episode_interface.dart';
import '../event/episode_event.dart';
import '../state/episode_state.dart';

class EpisodesViewModel extends Bloc<EpisodeEvent, EpisodeState> {
  final EpisodeInterface _episodeInterface;

  EpisodesViewModel({
    required EpisodeInterface repository,
  })  : _episodeInterface = repository,
        super(const EpisodeState()) {
    on<EpisodesFetched>(_mapEpisodesInitializedToState);
  }

  Future<void> _mapEpisodesInitializedToState(
    EpisodesFetched event,
    Emitter<EpisodeState> emit,
  ) async {
    try {
      emit(state.copyWith(status: EpisodeStatus.loading));

      final episodes = await _episodeInterface.getEpisodes(page: state.page);

      emit(state.copyWith(
        status: EpisodeStatus.loaded,
        episodes: [...episodes, ...state.episodes],
        page: state.page + 1,
      ));
    } on Exception catch (error) {
      emit(state.copyWith(status: EpisodeStatus.error, error: error));
    }
  }
}
