import 'package:flutter/material.dart';


class EpisodeCard extends StatelessWidget {
  const EpisodeCard({
    Key? key,
    required this.number,
    required this.name,
    required this.episode,
    required this.airDate
  }) : super(key: key);

  final int number;
  final String name;
  final String episode;
  final String airDate;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Text(number.toString()),
        title: Text(name),
        subtitle: Text(episode),
        trailing: Text(airDate),
      ),
    );
  }
}
