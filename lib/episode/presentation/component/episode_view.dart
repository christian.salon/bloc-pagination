// Create an episode view for viewing of episodes using bloc.
// Every time the user scrolls to the bottom of the list, the next page of episodes is fetched.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../event/episode_event.dart';
import '../state/episode_state.dart';
import '../view_model/episodes_view_model.dart';
import 'episode_card.dart';

class EpisodeView extends HookWidget {
  const EpisodeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scrollController = useScrollController();

    return Column(
      children: [
        _ScrollController(scrollController: scrollController),
        BlocBuilder<EpisodesViewModel, EpisodeState>(
          bloc: context.read<EpisodesViewModel>(),
          builder: (context, state) {
            return Expanded(
              child: ListView.builder(
                controller: scrollController,
                itemCount: state.episodes.length + 1,
                itemBuilder: (context, index) {
                  if (index >= state.episodes.length) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  final episode = state.episodes[index];
                  return EpisodeCard(
                    number: index + 1,
                    name: episode.name,
                    episode: episode.episode,
                    airDate: episode.airDate,
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }
}

class _ScrollController extends HookWidget {
  const _ScrollController({required this.scrollController});

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    void scrollListener() {
      if (scrollController.offset >=
          scrollController.position.maxScrollExtent) {
        context.read<EpisodesViewModel>().add(EpisodesFetched());
      }
    }

    useEffect(() {
      scrollController.addListener(scrollListener);
      return () => scrollController.removeListener(scrollListener);
    }, [scrollController]);

    return const SizedBox();
  }
}
