import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../data/model/output/episode.dart';

enum EpisodeStatus {
  intial,
  loading,
  loaded,
  error;

  bool get isInitial => this == EpisodeStatus.intial;
  bool get isLoading => this == EpisodeStatus.loading;
  bool get isLoaded => this == EpisodeStatus.loaded;
  bool get isError => this == EpisodeStatus.error;
}

@immutable
class EpisodeState extends Equatable {
  final EpisodeStatus status;
  final List<Episode> episodes;
  final int page;
  final Exception? error;

  const EpisodeState({
    this.status = EpisodeStatus.intial,
    this.episodes = const [],
    this.page = initialPage,
    this.error,
  });

  static const initialPage = 1;

  EpisodeState copyWith({
    EpisodeStatus? status,
    List<Episode>? episodes,
    int? page,
    Exception? error,
  }) {
    return EpisodeState(
      status: status ?? this.status,
      episodes: episodes ?? this.episodes,
      page: page ?? this.page,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [status, episodes, error];
}
